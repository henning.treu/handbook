---
title: About TeamOps
description: Learn more about TeamOps, the new people practice that brings precision and operations to how people work together.
canonical_path: "/handbook/teamops/"
images:
    - /images/opengraph/all-remote.jpg
cascade:
  type: docs
---
