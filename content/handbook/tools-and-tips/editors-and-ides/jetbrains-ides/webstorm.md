---
title: "Webstorm"
---

## Overview

Website: https://www.jetbrains.com/webstorm/

Best for: Javascript/Typescript and node.js applications

## Common Jetbrains Setup and Configuration

Jetbrains IDEs are standardized, so much of the setup and configuration information applies to all IDEs, and can be found under [Common Jetbrains Setup and Configuration](../setup-and-config).

Specific config for this RubyMine can be found below at [WebStorm-specific Setup and Configuration](webstorm-specific-setup-and-config)

## WebStorm-specific Setup and Configuration

Placeholder for future content...
